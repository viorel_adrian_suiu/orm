<?php
include "ORM/BaseEntity.php";
include "ORM/Utils.php";
include "ORM/EntityManager.php";
include "Entities/BlogPost.php";

$blogPost = new BlogPost();

$blogPost->setTitle('First post');
$blogPost->setContent('First content');


$em = new EntityManager();
$em->persist($blogPost);
$em->flush();

$blogPost->setTitle('Second Title');
$em->persist($blogPost);
$em->flush();

$em->remove($blogPost);
$em->flush();

var_dump($blogPost);

$blogPost2 = $em->getRepository(BlogPost::class)->find(5);
