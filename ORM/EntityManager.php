<?php

/**
 * Created by PhpStorm.
 * User: daniel-laptop
 * Date: 1/21/2019
 * Time: 8:36 PM
 */
class EntityManager
{

    /** @var BaseEntity[] */
    private $toBeRemoved =[];
    /** @var BaseEntity[] */
    private $toBeUpdated = [];
    /** @var BaseEntity[] */
    private $toBeCreated = [];
    /** @var mysqli */
    private $mysql;
    /** @var Repository[] */
    private $repositories = [];

    /**
     * EntityManager constructor.
     * @param $mysql
     */
    public function __construct()
    {
        $this->mysql = mysqli_connect('localhost', 'root', 'scoalait123', 'ORM');;
    }

    /**
     * @param $entityName
     * @return Repository
     */
    public function getRepository($entityName)
    {
        if (!isset($this->repositories[$entityName])){
            $this->repositories[$entityName] = new Repository($entityName, $this);
        }

        return $this->repositories[$entityName];
    }

    public function getTableName(BaseEntity $entity)
    {
        return Utils::fromCamelCase(get_class($entity));
    }


    public function persist(BaseEntity $entity){
        if ($entity->getId()){
            $this->toBeUpdated[$entity->getId()]=$entity;
        } else {
            $this->toBeCreated[spl_object_hash($entity)]=$entity;
        }
    }


    public function getData($entityName, $criteria)
    {
        $filter = '1=1';
        $table = $this->getTableName($entityName);
        foreach ($criteria as $column => $value) {
            $filter .= ' AND ' . "$column='$value'";
        }

        $result = $this->mysql->query("SELECT * FROM $table WHERE $filter;");
        return $result->fetch_all();
    }
    /**
     * @param BaseEntity $entity
     */
    public function remove(BaseEntity $entity){
        if ($entity->getId()){
            $this->toBeRemoved[$entity->getId()]=$entity;
        }
    }

    public function flush(){
        foreach ($this->toBeCreated as $createdEntity){
            $table=$this->getTableName($createdEntity);
            $columnsTable = '(`';
            $values = "('";
            foreach ($createdEntity as $columns=>$value) {
                if ($columns!= 'id') {
                    $columnsTable .= $columns;
                    $columnsTable .= "`,`";
                    if (is_array($value)){
                        $value=implode(',',$value);
                    }
                    $values .=$value;
                    $values .= "','";
                }

            }
            $columnsTable = substr($columnsTable, 0, -2);
            $values = substr($values, 0, -2);
            $columnsTable .= ')';
            $values .= ')';

            $this->mysql->query("INSERT INTO $table $columnsTable VALUES $values;");
            $createdEntity->setId($this->mysql->insert_id);
        }
        $this->toBeCreated = [];

        foreach ($this->toBeUpdated as $updateableEntity){
            $table=$this->getTableName($updateableEntity);
            $vals=get_object_vars($updateableEntity);
            $elements = [];
            foreach ($vals as $column => $value) {
                if (is_array($value)) {
                    $value=implode(", ", $value);
                }
                $elements[] = "$column='$value'";

            }
            $colUpdate = implode(',', $elements);
            $this->mysql->query("UPDATE $table SET $colUpdate WHERE id=".$updateableEntity->getId());
        }
        $this->toBeUpdated = [];

        foreach ($this->toBeRemoved as $removableEntity){
            $this->mysql->query("DELETE FROM ".$this->getTableName($removableEntity)." WHERE id=".$removableEntity->getId());
        }
        $this->toBeRemoved = [];

    }
}