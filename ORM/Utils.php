<?php

/**
 * Created by PhpStorm.
 * User: daniel-laptop
 * Date: 1/21/2019
 * Time: 8:12 PM
 */
class Utils
{
    /**
     * @param $input string
     * @return string
     */
    public static function fromCamelCase($input) {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode('_', $ret);
    }

    /**
     * @param $string string
     * @param bool $capitalizeFirstCharacter
     * @return string
     */
    public static function fromUnderscore($string, $capitalizeFirstCharacter = false)
    {

        $str = str_replace(' ', '', ucwords(str_replace('_', ' ', $string)));

        if (!$capitalizeFirstCharacter) {
            $str[0] = strtolower($str[0]);
        }

        return $str;
    }
}