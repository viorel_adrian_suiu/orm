<?php

/**
 * Created by PhpStorm.
 * User: daniel-laptop
 * Date: 1/21/2019
 * Time: 8:07 PM
 */
abstract class BaseEntity
{
    public $id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return BaseEntity
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }


}